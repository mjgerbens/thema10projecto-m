package nl.bioinf.mjgerbens.praatWI.Servlets;

import nl.bioinf.mjgerbens.praatWI.Models.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.Date;

/**
 * This program runs several provided scripts with the praat software.
 * It stores the data in a folder structure and returns the praat output
 * to the frontend.
 *
 * @author Olle de Jong & Menno Gerbens (&copy: 2018)
 * @version 1.0.0
 * @since 03-12-2018
 */
@WebServlet(name = "ThroughputServlet", urlPatterns = "/praatresults")
@MultipartConfig()

public class ThroughputServlet extends HttpServlet {

    private static final String UPLOAD_DIR = "Data/tmp";

    /**
     * Servlet which calls all the other models and returns the session results filepath to the frontend.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //gets absolute path of the web application and praat script/software
        String applicationPath = getServletContext().getInitParameter("uploadFolder");
        String praat_script = getServletContext().getInitParameter("praat_script");
        String praat_software = getServletContext().getInitParameter("praat_software");
        String split_channels = getServletContext().getInitParameter("split_channels");
        String chop_file = getServletContext().getInitParameter("chop_file");
        String normalize_file = getServletContext().getInitParameter("normalize_file");

        //catch checkbox options
        String s_bool = request.getParameter("split_audio");
        String c_bool = request.getParameter("chop_length");
        String n_bool = request.getParameter("normalize_60db");
        String o_bool = request.getParameter("run_over_orig");

        //receive webform parts
        String date = request.getParameter("Date");
        String patientID = request.getParameter("PatientID");

        // constructs path of the directory to save uploaded file
        String uploadFilePath = applicationPath + File.separator + UPLOAD_DIR;

        // creates the save directory if it does not exists
        File fileSaveDir = new File(uploadFilePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }

        String uploadFolder = fileSaveDir.getAbsolutePath();


        //Get the file parts from request and write it to the file on server
        String fileNameNorm = null;
        String fileNameD06 = null;

        Part part = request.getPart("WavFile_norm");
        fileNameNorm = ChangeFile.getFileName(part);
        part.write(uploadFilePath + File.separator + fileNameNorm);

        Part partD06 = request.getPart("WavFile_D06");
        fileNameD06 = ChangeFile.getFileName(partD06);
        partD06.write(uploadFilePath + File.separator + fileNameD06);


        //change the name of the files
        String[] renamedFiles = ChangeFile.rename(fileNameNorm, fileNameD06, date, patientID, uploadFilePath);
        ChangeFile.moveFile(uploadFilePath,renamedFiles[0]+".wav",applicationPath+"/Data/runtmp",applicationPath);
        ChangeFile.moveFile(uploadFilePath,renamedFiles[1]+".wav",applicationPath+"/Data/runtmp",applicationPath);

        //create folder structure
        File csvFile = FolderStructure.createFolderStructure(applicationPath, patientID);

        //run audiofile editors (the addition options if checked)
        String Addition = RunPraatScripts.checkBooleans(s_bool, c_bool, n_bool, o_bool, split_channels, chop_file, normalize_file, praat_software, uploadFilePath, patientID, praat_script, applicationPath);

        //Append PRAAT result to CSV file
        EditCSV.append(Addition, csvFile);

        //Create and append session results csv
        File sessionCSV = EditCSV.create(applicationPath+"/Data", "resultPG"+patientID);
        EditCSV.append(Addition, sessionCSV);
        String sessionCSVPath = sessionCSV.getAbsolutePath();

        //Remove (run)tmp map
        FolderStructure.deleteFolder(uploadFilePath);
        FolderStructure.deleteFolder(applicationPath+"/Data/runtmp");

        //set the filename and filepath attributes
        request.setAttribute("filepath", sessionCSVPath);
        request.setAttribute("filename", renamedFiles[0]);
        RequestDispatcher view = request.getRequestDispatcher("praat_result.jsp");

        //forward view
        view.forward(request,response);
    }

}
