package nl.bioinf.mjgerbens.praatWI.Models;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Creates folder structure
 */

public class FolderStructure {
    /**
     * Creates base folder structure
     *
     * @param uploadFolder
     * @param PatientID
     * @return
     * @throws IOException
     */

    public static File createFolderStructure(String uploadFolder, String PatientID) throws IOException {
        uploadFolder = uploadFolder + "/Data";
        //create Results folder
        String resultFolder = createFolder(uploadFolder, "Results");
        //create CSV file
        File csvFile = EditCSV.create(resultFolder, "resultPG"+PatientID);
        //create Orgineel folder
        createFolder(uploadFolder, "Origineel");
        //create tmp folder for running praat script
        createFolder(uploadFolder, "runtmp");
        //create Deelnemers folder
        String newPath = createFolder(uploadFolder, "Deelnemers");
        //create patient folder
        String patientPath = createFolder(newPath, "PG"+PatientID);
        createFolder(patientPath, "Splitted");
        createFolder(patientPath, "Normalized");
        createFolder(patientPath, "15Minutes");

        return csvFile;
    }

    /**
     * creates single folder.
     *
     * @param uploadFolder
     * @param folderName
     * @return
     */
    private static String createFolder(String uploadFolder, String folderName){
        File theDir = new File(uploadFolder+"/"+folderName);

        // if the directory does not exist, create it
        if (!theDir.exists()) {
            System.out.println("creating base directory: " + theDir.getName());
            boolean result = false;

            try{
                theDir.mkdir();
                result = true;
            }
            catch(SecurityException se){
                //handle it
            }
            if(result) {
                System.out.println("DIR created");
            }
        }
        uploadFolder = uploadFolder +"/"+ folderName;
        return uploadFolder;
    }

    /**
     * Deletes given folder.
     *
     * @param directory
     * @throws IOException
     */
    public static void deleteFolder(String directory)throws IOException {
        //Deletes folder
        FileUtils.deleteDirectory(new File(directory));
    }

}
