package nl.bioinf.mjgerbens.praatWI.Models;


import org.apache.commons.io.FileUtils;

import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * All the functions which changes file properties.
 */

public class ChangeFile {
    /**
     * renames filenames of given files.
     *
     * @param fileNameNorm
     * @param fileNameD06
     * @param date
     * @param patientID
     * @param uploadFilePath
     * @return
     */
    public static String[] rename(String fileNameNorm, String fileNameD06, String date, String patientID, String uploadFilePath){
        //get new names of files
        String[] renamedFiles = getNames(fileNameNorm, date, patientID);

        //change files
        changeFile(uploadFilePath, renamedFiles[0], fileNameNorm);
        changeFile(uploadFilePath, renamedFiles[1], fileNameD06);

        return renamedFiles;
    }

    /**
     * Creates filenames.
     * @param fileNameNorm
     * @param date
     * @param patientID
     * @return
     */

    private static String[] getNames(String fileNameNorm, String date, String patientID){
        //Change "-" in date string to "."
        String changedDate = date.replace("-", ".");

        //Extract 4 digit number from filename.
        Pattern pattern = Pattern.compile("(\\d{4})");
        Matcher matcher = pattern.matcher(fileNameNorm);
        String val = "";
        if (matcher.find()) {
            matcher.group(1);

            val = matcher.group(1);
        }

        //Create normal filename
        String baseFileName = new StringBuilder()
                .append("PG")
                .append(patientID)
                .append("_")
                .append(changedDate)
                .append("_")
                .append(val).toString();

        // create D06 filename
        String basefileNameD06 = baseFileName+"D06";

        //Add filenames to an array
        String[] renamedFiles = new String[2];
        renamedFiles[0] = baseFileName;
        renamedFiles[1] = basefileNameD06;

        return renamedFiles;
    }

    /**
     * Places renamed files in given folders
     *
     * @param uploadFilePath
     * @param renamedFile
     * @param fileName
     */

    private static void changeFile(String uploadFilePath, String renamedFile, String fileName){
        //Creates old and new filepaths.
        String oldFilePath = uploadFilePath + File.separator + fileName;
        String newFilePath = uploadFilePath + File.separator + renamedFile + ".wav";

        //Creates files with given filepaths
        File oldfile =new File(oldFilePath);
        File newfile =new File(newFilePath);

        //places renamed files to the wanted position
        if(!newfile.exists()) {
            oldfile.renameTo(newfile);
        }

    }

    /**
     * copy file to wanted folder.
     *
     * @param uploadFilePath
     * @param fileName
     * @param destination
     * @param applicationPath
     */
    public static void moveFile(String uploadFilePath, String fileName,String  destination, String applicationPath){
        //Creates source, destination and original folder filepaths
        File source = new File(uploadFilePath+"/"+fileName);
        File dest = new File(destination+"/"+fileName);
        File ori_dest = new File(applicationPath+"/Data/Origineel/"+fileName);

        //Copy files to destination
        try {
            FileUtils.copyFile(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Copy files to original folder
        try {
            FileUtils.copyFile(source, ori_dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Part of file download from webform.
     *
     * @param part
     * @return
     */
    public static String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2, token.length()-1);
            }
        }
        return "";
    }
}
