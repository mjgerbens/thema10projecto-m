package nl.bioinf.mjgerbens.praatWI.Servlets;

import nl.bioinf.mjgerbens.praatWI.Models.FolderStructure;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;

/**
 * This servlet returns the session results when called from within a .JSP file.
 */

@WebServlet(urlPatterns = "/DownloadResultServlet")
public class DownloadResultServlet extends HttpServlet {

	private final int ARBITARY_SIZE = 1048;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		response.setContentType("text/csv");
		String filename = request.getParameter("filename");

		response.setHeader("Content-disposition", "attachment; filename="+filename);
        String path = request.getParameter("filepath");
		//String outputFolder = getServletContext().getInitParameter("abs_path");

		try (InputStream in = new FileInputStream(path);
			 OutputStream out = response.getOutputStream()) {

			byte[] buffer = new byte[ARBITARY_SIZE];

			int numBytesRead;
			while ((numBytesRead = in.read(buffer)) > 0) {
				out.write(buffer, 0, numBytesRead);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}


        File file = new File(path);
		file.delete();
	}


}
