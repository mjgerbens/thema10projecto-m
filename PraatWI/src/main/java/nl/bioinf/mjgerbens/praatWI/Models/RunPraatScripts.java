package nl.bioinf.mjgerbens.praatWI.Models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;

/**
 * General model which runs several praat scripts.
 */
public class RunPraatScripts {

    /**
     * Runs small praat scripts to change the files.
     *
     * @param split_boolean
     * @param chop_boolean
     * @param normalize_boolean
     * @param over_orig_boolean
     * @param split_channels
     * @param chop_file
     * @param normalize_file
     * @param praat_software
     * @param uploadFilePath
     * @param patientID
     * @param praat_script
     * @param application
     * @return
     * @throws IOException
     */

    public static String checkBooleans(String split_boolean, String chop_boolean, String normalize_boolean, String over_orig_boolean,
                                     String split_channels, String chop_file, String normalize_file, String praat_software, String uploadFilePath,
                                     String patientID, String praat_script, String application) throws IOException {
        //initiate output
        String output_line = "";
        //Create patient id
        patientID = "PG" + patientID;

        //Run split channels script
        if (split_boolean != null) {
            run(split_channels, praat_software, uploadFilePath, patientID, praat_script);
        }
        //Run chop script
        if (chop_boolean != null) {
            run(chop_file, praat_software, uploadFilePath, patientID, praat_script);

        }
        //Run normalize file script
        if (normalize_boolean != null) {
            run(normalize_file, praat_software, uploadFilePath, patientID, praat_script);
        }
        //Create output
        String finalOutput = runfull(application + "/Data/runtmp", praat_script, praat_software, output_line);

        return finalOutput;
    }

    /**
     * runs the full praat script on the changed and original files.
     *
     * @param script
     * @param praat_software
     * @param upload
     * @param patient
     * @param praat_script
     * @throws IOException
     */
    private static void run(String script, String praat_software, String upload, String patient, String praat_script) throws IOException {
        //Create commandline process
        Runtime rt = Runtime.getRuntime();
        Process process = rt.exec(praat_software + " --run " + script + " " + upload + " " + patient);
        //Run script via commanline
        InputStreamReader reader =
                new InputStreamReader(process.getInputStream());

        BufferedReader buf_reader =
                new BufferedReader(reader);

        buf_reader.readLine();



    }

    public static String runfull(String uploadFolder,String praat_script, String praat_software, String output_line) throws IOException {
        String error = "error";
        try {
            //Create commandline process
            Runtime rt = Runtime.getRuntime();

            Process process = rt.exec(praat_software + " --run " + praat_script + " -25 2 0.3 0 "+ uploadFolder);

            InputStreamReader reader =
                    new InputStreamReader(process.getInputStream());

            BufferedReader buf_reader =
                    new BufferedReader(reader);

            //Write output
            String line;
            while ((line = buf_reader.readLine()) != null)
               output_line = output_line + '\n' + line;

            return output_line;
        } catch (IOException e) {
            System.out.println(e);

        }
        return error;
    }

}