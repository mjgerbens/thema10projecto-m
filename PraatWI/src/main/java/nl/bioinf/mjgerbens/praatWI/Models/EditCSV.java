package nl.bioinf.mjgerbens.praatWI.Models;

import java.io.*;

/**
 * Creates and appends to csv files
 */

public class EditCSV {
    /**
     * Creates csv file
     *
     * @param application
     * @param csvName
     * @return
     * @throws IOException
     */
    public static File create(String application, String csvName)throws IOException {
        //Create csv file
        File f = new File(application+File.separator+csvName+".csv");
        //Add header to file.
        if(!f.exists()){
            f.createNewFile();
            PrintWriter pw = new PrintWriter(f);
            pw.write("soundname,nsyll,npause,dur (s),phonationtime (s),speechrate (nsyll/dur),articulation rate (nsyll / phonationtime),ASD (speakingtime/nsyll)");
            pw.close();
        }else{
            System.out.println("File already exists");
        }
        //Close Outputstream
        new FileOutputStream(f, true).close();

        return f;

    }

    /**
     * Appends to wanted csv file.
     * @param new_rows
     * @param csvFile
     * @throws IOException
     */
    public static void append(String new_rows, File csvFile)throws IOException{
        //Open buffered writer
        FileWriter fw = new FileWriter(csvFile.getAbsoluteFile(), true);
        BufferedWriter bw = new BufferedWriter(fw);
        //Write new rows to csv file.
        bw.write(new_rows);
        bw.close();
    }


}
