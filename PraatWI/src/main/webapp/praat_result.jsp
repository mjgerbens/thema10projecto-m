<%--
  Created by IntelliJ IDEA.
  User: mjgerbens
  Date: 12-12-18
  Time: 9:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<!-- JSP INCLUDE: HEAD -->
<jsp:include page="head.jsp" >
    <jsp:param name="page_title" value="Results" />
</jsp:include >

<body>

<!-- JSP INCLUDE: NAVBAR-->
<jsp:include page="navbar.jsp" />
<div class="content_div2">
    <p>
        Download the result of the run<br>
        by clicking the button below.
    </p>
</div>
<form class="formm" method="post" action="DownloadResultServlet">
    <button class="submit_button2">Download CSV</button>
    <input type="hidden" name="filepath" id="filepath" class="filepath" value="${requestScope.filepath}">
    <input type="hidden" name="filename" id="filename" class="filename" value="${requestScope.filename}">
</form>


<!-- JSP INCLUDE: FOOTER -->
<jsp:include page="footer.jsp" />

</body>
</html>