<%--
  Created by IntelliJ IDEA.
  User: olledejong
  Date: 22/12/2018
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<!-- JSP INCLUDE: HEAD -->
<jsp:include page="head.jsp" >
    <jsp:param name="page_title" value="Run PRAAT" />
</jsp:include >

<body>

<!-- JSP INCLUDE: NAVBAR-->
<jsp:include page="navbar.jsp" />

<div class="content_div">
    <h3>HOW TO USE</h3>
    <p class="how_to_use">
        - Make sure you are on the homepage<br>
        - Fill out the required fields, which are: Patient ID and Date (of interview)<br>
        - Upload sound-file one: this is a normal, unedited sound-file.<br>
        - Upload sound-file two: this is a 6 db lower (D06) file.<br>
        (possible options: shorten to 15 min length checkbox, normaliseren <br>
        on 60db checkbox)<br>
        - Press run to start the analysis<br>
    </p>

</div>






<!-- JSP INCLUDE: FOOTER -->
<jsp:include page="footer.jsp" />


</body>
</html>
