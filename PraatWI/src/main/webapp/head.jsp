<%--
  Created by IntelliJ IDEA.
  User: olledejong
  Date: 22/12/2018
  Time: 17:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <title>${param.page_title}</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="generator" content="Geany 1.24.1"/>
    <link rel="stylesheet" type="text/css" href="css/main.css?v=2"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="js/file_upload.js"></script>
    <script src="js/loading_div.js"></script>
</head>
