<%--
  Created by IntelliJ IDEA.
  User: olledejong
  Date: 22/12/2018
  Time: 18:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<!-- JSP INCLUDE: HEAD -->
<jsp:include page="head.jsp" >
    <jsp:param name="page_title" value="About" />
</jsp:include >

<body>

<!-- JSP INCLUDE: NAVBAR-->
<jsp:include page="navbar.jsp" />


<div class="content_div">
 <p>
     This is a web application created for clinical usage.<br>
     By Olle de Jong and Menno Gerbens.<br>
     Students at Hanzehogeschool Groningen.

 </p>

</div>




<!-- JSP INCLUDE: FOOTER -->
<jsp:include page="footer.jsp" />

</body>
</html>
