/*
This JavaScript file shows a loading message
when the request to run PRAAT is initiated
 */

$(document).ready( function() {

    $('.praat_form').submit(function () {
    $('.load_text').css('visibility', 'visible');
});
});
