/*
This JavaScript file downsizes the name of the uploaded
file so that the name doesnt extend beyond the upload button
 */

$(document).ready( function() {

$('input[type="file"]').change( function() {
    //construct a selector for the label that belongs to the changed input field
    var label = 'label[for="' + $(this).attr('id') + '"]';

    // Get the filename
    var fn = $(this).val().split("\\")[2];


    // set the filename as the text for the label
    $(label).text("Uploaded file:\n" + fn);

    $(label).css("font-size", "16px");
}).hide();

});