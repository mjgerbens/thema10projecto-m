<%--
  Created by IntelliJ IDEA.
  User: mjgerbens
  Date: 12-12-18
  Time: 9:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<!-- JSP INCLUDE: HEAD -->
<jsp:include page="head.jsp" >
    <jsp:param name="page_title" value="Run PRAAT" />
</jsp:include >

<body>

<!-- JSP INCLUDE: NAVBAR-->
<jsp:include page="navbar.jsp"/>

<!-- DYNAMIC PART OF THIS PAGE -->
<div class="index">
    <div class="inputdiv">
        <div class="inputdiv_content">
            <h1>RUN PRAAT HERE</h1><br>
            <h2>Please fill in the fields below.<br>If something is unclear, please read the right part of this page.</h2>
            <br>
            <form name="processForm" class="praat_form" method="post" action="/praatresults", enctype="multipart/form-data">
                <input type="number" name="PatientID" placeholder="Patient ID" class="written_input" required>&nbsp;&nbsp;&nbsp;
                <input type="date" name="Date" class="written_input" placeholder="Date of the interview" required><br><br>
                <input type="file" name="WavFile_norm" id="WavFile_norm" accept=".wav" class="inputfile" required>
                <label for="WavFile_norm"><i class="fas fa-file-upload"></i>&nbsp;Choose a sound-file (normal)</label>&nbsp;&nbsp;&nbsp;
                <input type="file" name="WavFile_D06" id="WavFile_D06" accept=".wav" class="inputfile" required>
                <label for="WavFile_D06"><i class="fas fa-file-upload"></i>&nbsp;Choose a sound-file (D06)</label><br><br>

                <p class="checkbox_labels">
                    <label for="split_audio">Split audio-channels</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label for="chop_length">Shorten length to 15 min</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label for="normalize_60db">Normalize to 60 db</label><br>
                </p>

                <input type="checkbox" class="split_audio" id="split_audio" name="split_audio" value="Split audio-channels">

                <input type="checkbox" class="chop_length" id="chop_length" name="chop_length" value="Shorten length to 15 min">

                <input type="checkbox" class="normalize_60db" id="normalize_60db" name="normalize_60db" value="Normalize to 60 db"><br>


                <button type="submit" class="submit_button">GO <i class="fas fa-angle-double-right"></i></button>
            </form>
        </div>
    </div>
    <div class="how_to_use">
        <h3>HOW TO USE</h3>
        <p class="text_howto">
            - &nbsp;&nbsp;Make sure you are on the homepage<br>
            - &nbsp;&nbsp;Fill in the Patient ID and the date<br>
            - &nbsp;&nbsp;Upload sound-file one: <br>&nbsp;&nbsp;&nbsp;&nbsp;(this is a normal, unedited sound-file)<br>
            - &nbsp;&nbsp;Upload sound-file two: <br>&nbsp;&nbsp;&nbsp;&nbsp;(this is a 6 db lower (D06) file)<br>
            - &nbsp;&nbsp;Optionally: use one or more switches <br>&nbsp;&nbsp;&nbsp;&nbsp;for extra options<br>
            - &nbsp;&nbsp;Press GO to start the analysis<br>
        </p>
    </div>
</div>

<div class="load_text">
    loading... please be patient
</div>

<!-- JSP INCLUDE: FOOTER -->
<jsp:include page="footer.jsp" />

</body>
</html>
