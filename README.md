## **Web application for PRAAT**  

This web application is an easy to use alternative to running PRAAT and the  
"Praat Syllable Nuclei Script" from the command line. Fill out the required  
fields, upload two WAV files and select none, one or more extra options,  
hit RUN and analyse the output.  

### **PREREQUISITES**  
- Webbrowser  
- Tomcat version 9.0  
- IntelliJ IDEA  
- PRAAT  

### **USAGE**  
This web application can be ran using IntelliJ IDEA  
1) Clone this repository (top right of your browser window)  
2) Open the PraatWI project in IntelliJ  
3) Edit the web.xml file which is located in the WEB-INF folder to  
   the correct paths for your particular operating system;  
- praat_software:  
absolute path to praat software  
- praat_script:  
absolute path to praat syllable nuclei script  
- uploadFolder:  
absolute path to the folder which contains the cloned repository folder  
- split_channels:  
absolute path to the split-channels script which can be found inside the Docs folder  
- chop_file:  
absolute path to the chop-file script which can be found inside the Docs folder  
- normalize_file:  
absolute path to the normalize-file script which can be found inside the Docs folder  

3) Configure tomcat in IntelliJ settings  
4) Run the project by running index.jsp using Tomcat  
5) Fill in the information and upload files  
6) The results will be saved in folders for analysis  
    The session results will be downloadable from the webbrowser  

### **OUTPUT**  
The output will be saved in the created folder structure and will also  
be downloadable from the output page by clicking the "Download CSV" button.  
It contains the output from the "Praat Syllable Nuclei Script" with a clear  
labeled header and rowname. The rownames speak for them self,  
"norm_PG10_01.02.2019_0052.wav" means that it is the row with the results  
of the normalized version of the "PG10_01.02.2019_0052.wav" file. And  
"15min_PG10_01.02.2019_0052D06.wav" means that row contains the results of  
the original file that has been shortened to a length of 15 minutes. Also  
because of the end of the name you can tell it is the 6db lower file.

------------------------------------------------------------------------  
Authors: Olle de Jong & Menno Gerbens  
Acknowledgements:  
- Marcel Kempenaar (provided help when needed)  
- Alban Voppel (client; provided information about the goal of the application)  
