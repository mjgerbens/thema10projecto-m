form split_channels    
	sentence directory /directory
	sentence patientID patient
endform

Create Strings as file list... list 'directory$'/*.wav
numberOfFiles = Get number of strings
for ifile to numberOfFiles
   select Strings list
   fileName$ = Get string... ifile
   Read from file... 'directory$'/'fileName$'
	sound_name$ = selected$ ("Sound")
	Write to WAV file... ../../Data/Origineel/'fileName$'

	select Sound 'sound_name$'
	endTime = Get end time
	if endTime >= 900.00
		Extract part: 0.00, 900.00, "rectangular", 1, 0
	endif	
	Save as WAV file: "'directory$'/../runtmp/15min_'fileName$'"
	Save as WAV file: "'directory$'/../Deelnemers/'patientID$'/15Minutes/15min_'fileName$'"

	select all
	minus Strings list
	Remove

endfor
